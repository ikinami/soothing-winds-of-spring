extends Control

@export var entities : Array[PackedScene]

var right_down = false
var mouse_inside = false
const zoom_rate = 1.3

var dropped_count = {}

signal unlock_placables
signal achievement
signal cantplace

enum types {
	GRASS = 0,
	DIRT = 1,
	WATER = 3,
	STONE = 2,
	FLOWER = 100,
	FISHIES = 101,
	SEEDS = 102,
	BEE = 1000,
	BUTTERFLY = 1001,
	BIRB = 1002,
	DUCK = 1003,
	TREE = 1004,
	NEST = 1005,
	FOX = 1006,
	HOG = 1007,
}

var unlocks = [types.FLOWER, types.SEEDS, types.FISHIES]

func _can_drop_data(at_position, data):
	return true

func add_unlock(unlock):
	if not unlocks.has(unlock):
		unlocks.push_back(unlock)
		emit_signal("unlock_placables", unlock, unlocks)
		return true
	return false

func check_unlocks():
	if dropped_count[types.FLOWER] > 5:
		add_unlock(types.BEE)
		if add_unlock(types.BUTTERFLY):
			emit_signal("achievement", "You have planted a lot of flowers, they attracted butterflies and bees!")
	if dropped_count[types.FISHIES] > 5:
		if add_unlock(types.DUCK):
			emit_signal("achievement", "You have a lot of fish, they attracted ducks!")
	if dropped_count[types.TREE] > 5:
		if add_unlock(types.HOG):
			emit_signal("achievement", "The fruit that fell from your trees attracted some wild boars!")
	if dropped_count[types.BEE] > 10 and dropped_count[types.BIRB] > 5:
		if add_unlock(types.TREE):
			emit_signal("achievement", "The bees and the birds made it possible for trees to grow!")
	if dropped_count[types.TREE] > 10 and dropped_count[types.DUCK] > 5:
		if add_unlock(types.NEST):
			emit_signal("achievement", "Your ducks are now laying eggs in nests!")
	if dropped_count[types.NEST] > 5:
		if add_unlock(types.FOX):
			emit_signal("achievement", "The eggs have attracted foxes!")
	if dropped_count[types.SEEDS] > 5:
		if add_unlock(types.BIRB):
			emit_signal("achievement", "The seeds seem to attract the birds!")
	if dropped_count[types.HOG] > 5:
		if add_unlock(types.DIRT):
			emit_signal("achievement", "Your hogs can boars up grass now and turn it into dirt!")
	if dropped_count[types.BUTTERFLY] > 5:
		if add_unlock(types.GRASS):
			emit_signal("achievement", "Your butterflies are spreadying the grass!")
	

func _ready():
	for elem in types:
		dropped_count[types[elem]] = 0
	get_tree().get_root().size_changed.connect(clamp_world_position)
	emit_signal("unlock_placables", types.FLOWER, unlocks)

func _drop_data(at_position, data):
	if data.to_id >= 1000:
		if $World/CanvasLayer/TileMap.check_compatible_placement(at_position, data.to_id):
			emit_signal("cantplace")
			return
		var new_entity = entities[data.to_id-1000].instantiate()
		new_entity.position = $World/entities.to_local(at_position)
		new_entity.z_index = new_entity.position.y/100+1000
		$World/entities.add_child(new_entity)
		if dropped_count.has(data.to_id):
			dropped_count[data.to_id] += 1
		else:
			dropped_count[data.to_id] = 1
	elif $World/CanvasLayer/TileMap.set_tile_at(at_position, data.to_id, data.layer):
		if dropped_count.has(data.to_id):
			dropped_count[data.to_id] += 1
		else:
			dropped_count[data.to_id] = 1
	else:
		emit_signal("cantplace")
	check_unlocks()

func zoom(rate):
	scale *= rate
	$World/CanvasLayer/TileMap.scale *= rate
	

func _input(event):
	if event is InputEventMouseButton and mouse_inside:
#		if event.is_pressed():
#			if event.button_index == MOUSE_BUTTON_WHEEL_UP:
#				zoom(zoom_rate)
#			elif event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
#				zoom(1/zoom_rate)
#			else:
#				right_down = event.is_pressed()
#		else:
			right_down = event.is_pressed()
	if event is InputEventMouseMotion and right_down and mouse_inside:
		$World.position += event.relative
		clamp_world_position()

func clamp_world_position():
	$World.position = $World.position.clamp(-$bottom_right.get_global_transform().origin + get_viewport_rect().size, -$top_left.get_global_transform().origin)
	$World/CanvasLayer/TileMap.position = $World.position * scale.x

func _on_mouse_entered():
	mouse_inside = true


func _on_mouse_exited():
	mouse_inside = false
