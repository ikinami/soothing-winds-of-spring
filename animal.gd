extends "track_to_tile.gd"

var progress = -1

@export var entity_to_check_for  = 0

func set_moving(is_moving):
	$animal.rotation = (position - target).angle()
	if (target - position).x < 0:
		$animal.scale.y = 1
	else:
		$animal.scale.y = -1
	if is_moving:
		progress = 0
	else:
		progress = -1
		$animal.rotation = 0
		$animal.scale.y = 1

func get_new_target():
	var entities = get_parent().get_children()
	var closest = Vector2(10000,0)
	for child in entities:
		if child.get("entity_type") != entity_to_check_for:
			continue
		if (position - child.position).length() < closest.length() and (child.position - position).length() > 0.1:
			closest = child.position - position
	if closest == Vector2(10000,0):
		closest = Vector2(randi_range(-5,5),randi_range(-5,5))
	target = position+closest
	timer_active = false


func _process(delta):
	if progress >= 0:
		progress += delta
		$animal.rotation += (cos(progress*5))/100
