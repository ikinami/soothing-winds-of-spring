extends Node2D

var target = null

@onready var tilemap = get_parent().get_parent().get_node("./CanvasLayer/TileMap")
@export var tile_to_track = 100

var timer_active = false

# Called when the node enters the scene tree for the first time.
func _ready():
	get_new_target()
	set_moving(true)

func set_moving(is_moving):
	pass

func get_new_target():
	var surrounding_tiles = tilemap.get_surrounding_tiles(get_global_transform().origin, 1)
	var closest = null
	for x in range(-5,5):
		for y in range(-5,5):
			if Vector2i(x,y) == Vector2i.ZERO:
				continue
			if surrounding_tiles[x+5][y+5].custom_data_0 == tile_to_track and (
				closest == null or closest.length() > Vector2i(x,y).length()
			):
				closest = Vector2i(x,y)
	if closest == null:
		closest = Vector2i(randi_range(-5,5),randi_range(-5,5))
	var own_position = tilemap.local_to_map(tilemap.to_local(get_global_transform().origin))
	target = tilemap.map_to_local(own_position+closest)
	timer_active = false

func _physics_process(delta):
	var to_target : Vector2i = target - position
	if to_target.length() < 10 and not timer_active:
		timer_active = true
		set_moving(false)
		await get_tree().create_timer(randf_range(5,10)).timeout
		get_new_target()
		set_moving(true)
	else:
		position += (target - position) * delta
		z_index = position.y/100+1000
