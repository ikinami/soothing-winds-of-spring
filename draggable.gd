extends Panel

enum types {
	GRASS = 0,
	DIRT = 1,
	WATER = 3,
	STONE = 2,
	FLOWER = 100,
	FISHIES = 101,
	SEEDS = 102,
	BEE = 1000,
	BUTTERFLY = 1001,
	BIRB = 1002,
	DUCK = 1003,
	TREE = 1004,
	NEST = 1005,
	FOX = 1006,
	HOG = 1007,
}

@export var tile_id = types.GRASS
@export var layer = 1
@export var texture : Texture

func _ready():
	$item.texture = texture

func make_visible():
	$qm.visible = false
	$item.visible = true

func _get_drag_data(at_position):
	if not $item.visible:
		return
	set_drag_preview(duplicate())
	return {"to_id": tile_id, "layer": layer}
