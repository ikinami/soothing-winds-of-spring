extends VBoxContainer

func set_unlocks(_unlock, unlocks):
	for child in get_children():
		if child.tile_id in unlocks:
			child.make_visible()
