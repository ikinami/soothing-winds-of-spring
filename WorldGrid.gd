extends TileMap

enum types {
	GRASS = 0,
	DIRT = 1,
	WATER = 3,
	STONE = 2,
	FLOWER = 100,
	FISHIES = 101,
	SEEDS = 102,
	BEE = 1000,
	BUTTERFLY = 1001,
	BIRB = 1002,
	DUCK = 1003,
	TREE = 1004,
	NEST = 1005,
	FOX = 1006,
	HOG = 1007,
}

var tiles = {
	types.GRASS: Vector2i(0,0),
	types.DIRT: Vector2i(0,1),
	types.STONE: Vector2i(1,1),
	types.FLOWER: Vector2i(0,3),
	types.FISHIES: Vector2i(0,4),
	types.SEEDS: Vector2i(2,1),
}

var compatible_ids = {
	types.FLOWER: types.GRASS,
	types.SEEDS: types.DIRT,
	types.GRASS: types.DIRT,
	types.FISHIES: types.WATER,
	types.DIRT: types.GRASS,
	types.TREE: types.GRASS,
	types.DUCK: types.WATER,
	types.BEE: types.GRASS,
	types.BIRB: types.GRASS,
	types.NEST: types.GRASS,
	types.BUTTERFLY: types.GRASS,
	types.FOX: types.GRASS,
	types.HOG: types.GRASS,
}

func check_compatible_placement(coords, to_id):
	return compatible_ids[to_id] != get_cell_tile_data(0, local_to_map(to_local(coords))).custom_data_0

func set_tile_at(coords, to_id, layer=0):
	if check_compatible_placement(coords, to_id):
		return false
	set_cell(layer, local_to_map(to_local(coords)), 0, tiles[to_id])
	return true


func get_surrounding_tiles(coords, layer=1):
	var tile_coords = local_to_map(to_local(coords))
	var res_tiles = []
	for x in range(-5,5):
		res_tiles.push_back([])
		for y in range(-5,5):
			var value = get_cell_tile_data(layer, tile_coords+Vector2i(x,y))
			if value == null:
				value = {"custom_data_0":-1}
			res_tiles[x+5].push_back(value)
	return res_tiles
