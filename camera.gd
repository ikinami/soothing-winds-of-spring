extends Camera2D

const zoom_rate = 2

func _ready():
	make_current()

func _input(event):
	print(zoom)
	if event is InputEventMouseButton:
		if event.is_pressed():
			# zoom in
			if event.button_index == MOUSE_BUTTON_WHEEL_UP:
				zoom *= zoom_rate
				# call the zoom function
			# zoom out
			if event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
				zoom /= zoom_rate
