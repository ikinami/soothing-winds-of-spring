extends Panel

func _ready():
	display_message("Welcome to \"Soothing Winds of Spring\"!
In this game there are no goals or objectives, no scoreboards or time limits.
Just an environment and some cute little plants and animals to place in it.
You can place things by dragging and dropping them onto the map.
Some things can only be placed on certain tiles! Experiment if you can't place something!
Placing a few of certain plants or animals unlocks new ones.
Try to unlock them all! Or don't. What matters is you build a cozy little world you feel at home in!
Messy source code published under: https://gitlab.com/ikinami/soothing-winds-of-spring
Made with love by: Kinami Imai (https://ikinami.gitlab.io/kinami/)
Game engine: Godot 4
Music credits: \"Ancient Winds\" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 4.0 License
http://creativecommons.org/licenses/by/4.0/")

func display_message(message):
	$text.text = message
	show()
