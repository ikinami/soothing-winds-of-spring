extends "track_to_tile.gd"

func set_moving(is_moving):
	$birb.rotation = (position - target).angle()
	if (target - position).x < 0:
		$birb.scale.y = 1
	else:
		$birb.scale.y = -1
	$birb.visible = is_moving
	$birb_sitting.visible = not is_moving
